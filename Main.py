#!/usr/bin/python3

# -
# | TGSSpy
# | Shamelessly spying on Telegram users' online status
# |
# | Copyright (C) 2021, OctoSpacc
# | Licensed under the AGPLv3
# -

import json
from pyrogram import Client
from time import sleep

RunningInfo = {}

try:
	with open ("Config.json", "r") as ConfigFile:
		Config = json.load(ConfigFile)
except:
	print("[E] Error reading Config.json.\nDid you create it from template, with correct formatting?\nExiting. ")
	exit()

ClientAPI = Client("TGSSpy", Config["Client API Config"]["API ID"], Config["Client API Config"]["API Hash"])
with ClientAPI:
		SelfID = ClientAPI.get_users("self")["id"]

print("[I] Starting TGSSpy...")

def DiscoverPublicGroups(TargetID):
	with ClientAPI:
		try:
			print("[D] Contacting @tgscanrobot to discover " + str(TargetID) + "'s public groups.")
			ClientAPI.send_message("@tgscanrobot", str(TargetID))
			sleep(1)
			DiscoverResponse = ClientAPI.get_history("@tgscanrobot", limit=1)[0]["text"]
			sleep(1)
		except:
			return None

	if DiscoverResponse.startswith("Human found!"):
		DiscoveredPublicGroups = []

		for DiscoveredPublicGroup in DiscoverResponse.split("\n - @")[1:]:
			DiscoveredPublicGroups += [DiscoveredPublicGroup[0:DiscoveredPublicGroup.find(" ")]]

		return DiscoveredPublicGroups

	else:
		return None

def Main():
	while True:
		TargetsList = []
		TargetsStatusesList = []
		TargetsNonSingleChat = []
		TargetsNonSingleChatList = []
		TargetsNonSingleChatIDs = []

		print("[D] Checking statuses.")
		for Target in Config["Targets"]:
			TargetsList += [Target["ID"]]

		for NonSingleChat in Config["Target Non-Single Chats"]:
			with ClientAPI:
				TargetsNonSingleChat += ClientAPI.get_chat_members(NonSingleChat["ID"])
				for TargetNonSingleChat in TargetsNonSingleChat:
					#TargetsNonSingleChatIDs += [TargetNonSingleChat["user"]["id"]]
					if TargetNonSingleChat["user"]["id"] not in TargetsList and TargetNonSingleChat["user"]["username"] not in TargetsList:
						TargetsList += [TargetNonSingleChat["user"]["id"]]

		with ClientAPI:
			TargetsInfo = ClientAPI.get_users(TargetsList)

		CurrentIndex = 0
		while CurrentIndex < len(TargetsList)-1:
			TargetsStatusesList += [TargetsInfo[CurrentIndex]["status"]]
			CurrentIndex += 1

		TargetIndex = 0
		while TargetIndex < len(TargetsList):
			if TargetsList[TargetIndex] in RunningInfo and RunningInfo[TargetsList[TargetIndex]] != TargetsStatusesList[TargetIndex] and TargetsList[TargetIndex] != SelfID:
				if len(Config["Targets"]) > TargetIndex and "Custom Name" in Config["Targets"][TargetIndex]:
					TargetName = str(Config["Targets"][TargetIndex]["Custom Name"])
				else:
					TargetName = str(TargetsList[TargetIndex])

				if str(TargetsList[TargetIndex]).isnumeric():
					TargetNameFormatted = "[" + str(TargetName) + "](tg://user?id=" + str(TargetsList[TargetIndex]) + ")"
				else:
					if str(TargetsList[TargetIndex]).startswith("+"):
						TargetNameFormatted = TargetName
					else:
						TargetNameFormatted = "[" + TargetName + "](https://t.me/" + str(TargetsList[TargetIndex]).replace("@", "") + ")"

				if TargetsStatusesList[TargetIndex] != "online" and TargetsStatusesList[TargetIndex] != "offline":
					# alternative methods to detect a user with privacy go here
					pass

				else:
					if TargetsStatusesList[TargetIndex] == "online":
						TargetStatusMessage = "🌐 " + TargetName + " is online."
						TargetStatusMessageFormatted = "🌐 " + TargetNameFormatted + " is online."
					elif TargetsStatusesList[TargetIndex] == "offline":
						TargetStatusMessage = "📵 " + TargetName + " went offline."
						TargetStatusMessageFormatted = "📵 " + TargetNameFormatted + " went offline."

					print("[I] " + TargetStatusMessage)
					print("[D] Letting monitor(s) know.")

					for Monitor in Config["User Preferences"]["Monitors"]:
						with ClientAPI:
							ClientAPI.send_message(Monitor, TargetStatusMessageFormatted, disable_web_page_preview=True)

			TargetIndex += 1

		TargetIndex = 0
		while TargetIndex < len(TargetsList)-1:
			RunningInfo.update({TargetsList[TargetIndex]: TargetsStatusesList[TargetIndex]})
			TargetIndex += 1

		print("[D] Sleeping...")
		sleep(Config["User Preferences"]["Refresh Time"])

if __name__ == "__main__":
	try:
		Main()
	except KeyboardInterrupt:
		print("[I] KeyboardInterrupt received, exiting.")
		exit()